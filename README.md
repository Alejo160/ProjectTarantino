ENTORNO EDUCATIVO PARA ENSEÑANZA Y PRÁCTICA DE PROGRAMACIÓN BAJO USO DE JAVASCRIPT PARA LOS ESTUDIANTES DE INGENIERIA DE LA UNIVERSIDAD VALLE DEL MOMBOY

Clase 1:


	Título:

		Hola mundo!
	Código:

		//Introduccion:
		alert("Hola Mundo!");
	Documentación:
	
		Para nuestro primer programa en Javascript, basta con entender como vamos a trabajar de acá en adelante.
		En este caso, utilizaremos la instrucción "alert()", que nos permite mostrar un mensaje en la pantalla del usuario, si haces click en el boton "Correr", podrás ver como aparece en tu navegador, un aviso diciendo "Hola Mundo!". Te invito a cambiar el mensaje para mostrar tu nombre, o incluso, mostrar algún otro mensaje que prefieras.

Clase 2:
	
	Título:

		Variables
	Código:

		var Variable_1; //Utilizaremos la palabra var para indicar que estamos declarando una variable seguida del nombre de la variable

		var Variable_2 = 5; // Si cuando se declara una variable se le asigna también un valor, se dice que la variable ha sido inicializada
	
		// En JavaScript no es obligatorio inicializar las variables, o sea, que podemos declararlas en algun momento, y asignarles un valor más adelante.
	Documentación:

		En este ejercicio, comenzaremos con el uso de variables, que en los lenguajes de programación siguen una lógica similar a las variables utilizadas en otros ámbitos como las matemáticas. Una variable es un elemento que se emplea para almacenar y hacer referencia a otro valor.

		El nombre de una variable también se conoce como identificador y debe cumplir las siguientes normas:
		
		Sólo puede estar formado por letras, números y los símbolos $ (dólar) y _ (guión bajo).
		
		El primer carácter no puede ser un número.

		Si deseas comprobar el contenido de alguna de las variables que declaramos, puedes hacerlo con la instrucción alert() que aprendimos en la lección anterior. 

Clase 3:

	Título:

		Tipos de Variables
	Código

		var iva = 16; 
		// Variable tipo entero
		

		var total = 234.65; 
		// Variable tipo decimal
		
		var dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado","Domingo"];
		// Variable tipo array, Un array es una colección de variables, más adelante trabajaremos el uso de estos.

		var clienteRegistrado = false;
		var ivaIncluido = true;
		// Variables de tipo Boolean.
	Documentación:

		Sin importar que las variables en JavaScript se declaran todas de la misma manera, utilizando la palabra reservada var , la forma en la que se les asigna un valor depende del tipo de valor que se quiere almacenar (números, textos, etc.)

		Como ejercicio propuesto, puedes crear un array llamado meses y que almacene el nombre de los doce meses del año y mostrar por pantalla los doce nombres utilizando la función alert().

Clase 4:

	Título:

		Operadores: Introducción.
	Código:

		var numero1 = 3;
		var numero2 = 4;

		numero1 = 5;
		// Ahora, la variable numero1 vale 5
	
		numero1 = numero2;
		// Ahora, la variable numero1 vale 4
	
		var numero_incremento = 5;
		numero_incremento++;

		alert("Numero_incremento = "+numero_incremento); // numero_incremento = 6

		var numero_decremento = 14;
		numero_decremento++;

		alert("Numero_decremento = "+numero_decremento); // numero_decremento = 13
	Documentación:

		Los operadores permiten manipular el valor de las variables, realizar operaciones matemáticas con sus valores y comparar diferentes variables. De esta forma, los operadores permiten a los programas realizar  cálculos complejos y tomar decisiones lógicas en función de comparaciones y otros tipos de condiciones.


		Asignación
		
		El operador de asignación es el más utilizado y el más sencillo. Este operador se utiliza	para guardar un valor específico en una variable.	A la izquierda del operador, siempre debe indicarse el nombre de una variable. A la derecha del operador, se pueden indicar variables, valores, condiciones lógicas, etc.

		Incremento y decremento
		Estos dos operadores solamente son válidos para las variables numéricas y se utilizan para incrementar o decrementar en una unidad el valor de una variable.
		El operador de incremento se indica mediante el prefijo ++ en el nombre de la variable, De forma equivalente, el operador decremento (indicado como un prefijo -- en el nombre de la variable)

Clase 5:

	Título:

		Operadores Lógicos

	Código:

		////////////////
		//	Negación //
		///////////////
		var visible = true;
		alert("La negación de visible es = "+!visible); 
		// Muestra "false" en lugar de "true"

		//////////
		//	AND //
		//////////
		
		var Verdadero_1 = true;
		var Falso_1 = false;
		var Verdadero_2 = true;
		var Falso_2 = false;
		
		resultado_1 = Verdadero_1 && Falso_1;
		alert("Un valor Verdadero && un valor Falso arroja como resultado = "+resultado_1);
		
		resultado_2 = Verdadero_1 && Verdadero_2;
		alert("Un valor Verdadero && otro valor Verdadero arroja como resultado = "+resultado_2);
		
		////////
		// OR //
		///////
		
		resultado_3 = Verdadero_1 || Falso_1;
		alert("Un Valor verdadero || un valor Falso arroja como resultado = "+resultado_3);

		resultado_4 = Falso_1 || Falso_2
		alert("Un Valor Falso || un valor Falso arroja como resultado = "+resultado_4);

	Documentación:

		Los operadores lógicos son imprescindibles para realizar aplicaciones complejas, ya que se utilizan para tomar decisiones sobre las instrucciones que debería ejecutar el programa en función de ciertas condiciones. El resultado de cualquier operación que utilice operadores lógicos siempre es un valor lógico o booleano.

	 	Negación

		Uno de los operadores lógicos más utilizados es el de la negación. Se utiliza para obtener el valor contrario al valor de la variable. La negación lógica se obtiene prefijando el símbolo ! al identificador de la variable.

		AND 

		La operación lógica AND obtiene su resultado combinando dos valores booleanos. El operador se indica mediante el símbolo && y su resultado solamente es true si los dos operandos son true.

		OR

		La operación lógica OR también combina dos valores booleanos. El operador se indica mediante el símbolo || y su resultado es true si alguno de los dos operandos es true

Clase 6:

	Título:
		
		Operadores Matemáticos

	Código:

		var numero1 = 10;
		var numero2 = 5;
		var numero3 = 9;

		resultado_1 = numero1 / numero2;
		resultado_2 = 3 + numero1;
		resultado_3 = numero2 - 4;
		resultado_4 = numero1 * numero2;
		resultado_5 = numero1 % numero2;
		resultado_6 = numero3 % numero2;
		
		alert(	"numero1 = "+numero1+"\n"
		+"numero2 = "+numero2+"\n"
		+"numero3 = "+numero3+"\n \n"
		+"numero_1 / numero_2 = "+resultado_1+"\n"
		+"3 + numero1 = "+resultado_2+ "\n"
		+"numero2 - 4 = "+resultado_3+"\n"
		+"numero1 * numero2 = "+resultado_4+"\n"
		+"numero1 % numero2 = "+resultado_5+"\n"
		+"numero3 % numero2 = "+resultado_6);

		var numero4 = 10;
		numero4 += 3; // numero4 = numero4 + 3 = 13

		var numero5 = 17;
		numero5 -= 1; // numero5 = numero5 - 1 = 16

		var numero6 = 4;
		numero6 *= 2; // numero6 = numero6 * 2 = 8

		var numero7 = 20;
		numero7 /= 5; // numero7 = numero7 / 5 = 4

		var numero8 =25;
		numero8 %= 4; // numero8 = numero8 % 4 = 1

	Documentación:

		JavaScript permite realizar manipulaciones matemáticas sobre el valor de las variables numéricas. Los operadores definidos son: suma (+), resta (-), multiplicación (*) y división (/).
		
		Además de los cuatro operadores básicos, JavaScript define otro operador matemático que no es sencillo de entender cuando se estudia por primera vez, pero que es muy útil en algunas ocasiones. Se trata del operador "módulo", que calcula el resto de la división entera de dos números. Si se divide por ejemplo 10 y 5, la división es exacta y da un resultado de 2. El resto de esa división es 0, por lo que módulo de 10 y 5 es igual a 0. Sin embargo, si se divide 9 y 5, la división no es exacta, el resultado es 1 y el resto 4, por lo que módulo de 9 y 5 es igual a 4.
		
		Los operadores matemáticos también se pueden combinar con el operador de asignación para abreviar su notación. Para comprobar esto, puedes enviar un mensaje y verificar el contenido de las variables numero4, numero5, numero6, numero7 y numero8.

		Como un adiucional, esta vez, hemos añadido algo a nuestro mensaje de alerta, el salto de linea, que se añade con los caracteres \n dentro de comillas simples. 

Clase 7:

	Título:

		Operadores Relacionales

	Código:

		var numero1 = 3;
		var numero2 = 5;

		resultado = numero1 > numero2; 
		// resultado = false
		resultado = numero1 < numero2; 
		// resultado = true

		numero3 = 7;
		numero4 = 7;

		resultado = numero3 >= numero4;
		// resultado = true
		resultado = numero3 <= numero4;
		// resultado = true
		resultado = numero3 == numero4;
		// resultado = true
		resultado = numero3 != numero4;
		// resultado = false


		var texto1 = "hola";
		var texto2 = "hola";
		var texto3 = "adios";
		resultado = texto1 == texto3; // resultado = false
		resultado = texto1 != texto2; // resultado = false
		resultado = texto3 >= texto2; // resultado = false

	Documentación:
		Los operadores relacionales definidos por JavaScript son idénticos a los que definen las matemáticas: mayor que (>), menor que (<), mayor o igual (>=), menor o igual (<=), igual que (==) y distinto de (!=). Los operadores que relacionan variables son imprescindibles para realizar cualquier aplicación compleja; y el resultado de todos estos operadores siempre es un valor booleano. 

		Se debe tener especial cuidado con el operador de igualdad (==), ya que es el origen de la mayoría de errores de programación, incluso para los usuarios que ya tienen cierta experiencia desarrollando scripts. El operador == se utiliza para comparar el valor de dos variables, por lo que es muy diferente del operador =, que se utiliza para asignar un valor a una variable

		Cuando se utilizan cadenas de texto, los operadores "mayor que" (>) y "menor que" (<) siguen un razonamiento no intuitivo: se compara letra a letra comenzando desde la izquierda hasta que se encuentre una diferencia entre las dos cadenas de texto. Para determinar si una letra es mayor o menor que otra, las mayúsculas se consideran menores que las minúsculas y las primeras letras del alfabeto son menores que las últimas (a es menor que b, b es menor que c, A es menor que a, etc.)

Clase 8:

	Título:

		Estrcturas de control de flujo - Estructura if (Parte 1)

	Código:
	
		var mostrarMensaje = true;
		
		if(mostrarMensaje) {
			alert("Hola Mundo");
		}


	Documentación:

		Los programas que se pueden realizar utilizando solamente variables y operadores son una simple sucesión lineal de instrucciones básicas. Sin embargo, no se pueden realizar programas que muestren un mensaje si el valor de una variable es igual a un valor determinado y no muestren el mensaje en el resto de casos. Tampoco se puede repetir de forma eficiente una misma instrucción, como por ejemplo sumar un determinado valor a todos los elementos de un array.
		Para realizar este tipo de programas son necesarias las estructuras de control de flujo, que son instrucciones del tipo "si se cumple esta condición, hazlo; si no se cumple, haz esto otro". También existen instrucciones del tipo "repite esto mientras se cumpla esta condición". Si se utilizan estructuras de control de flujo, los programas dejan de ser una sucesión lineal de instrucciones para convertirse en programas inteligentes que pueden tomar decisiones en función del valor de las variables.

		Estructura if
		
		La estructura más utilizada en JavaScript y en la mayoría de lenguajes de programación es la estructura if. Se emplea para tomar decisiones en función de una condición. Su definición formal es:

		if(condicion) {
			...
		}

		Si la condición se cumple (es decir, si su valor es true) se ejecutan todas las instrucciones que se encuentran dentro de {...}. Si la condición no se cumple (es decir, si su valor es false) no se ejecuta ninguna instrucción contenida en {...} y el programa continúa ejecutando el resto de instrucciones del script.

		En el ejemplo que aparece en el editor, el mensaje sí que se muestra al usuario ya que la variable mostrarMensaje tiene un valor de true y por tanto, el programa entra dentro del bloque de instrucciones del if.
		
Clase 9:

	Título:
	
		Estructura if (Parte 2)

	Código:
	
		var mostrarMensaje = true;
		var mensaje = confirm("Desea aprobar la solicitud?");

		if(mostrarMensaje === true && mensaje) {
			alert("La solicitud fué aprobada");
		}
		if (mostrarMensaje === true && !mensaje){
		    alert("La solicitud fué rechazada");
		}

	Documentación:
		
		Esta vez hemos añadido un par de detalles a nustro código. Por ejemplo, la instrucción confirm, que le presenta al usuario la opcion de aceptar o cancelar, almacenando el resultado en una variable de tipo booleano ( aceptar = true , cancelar = false). Además de eso, la triple comparación === es recomendada cuando se comparan valores de tipo booleano; agreagamos un caso para cada una de las respuestas del usuario, y demostramos que podemos utilizar multiples operadores como AND y OR para encadenarlos y crear condiciones complejas.	

Clase 10:

	Título:

		Estructura if (parte 3)

	Código:
		var edad = 18;
		
		if(edad >= 18) {
			alert("Eres mayor de edad");
		}
		else {
			alert("Todavía eres menor de edad");
		}

	Documentación:

		En ocasiones, las decisiones que se deben realizar no son del tipo "si se cumple la condición, hazlo; si no se cumple, no hagas nada". Normalmente las condiciones suelen ser del tipo "si se cumple esta condición, hazlo; si no se cumple, haz esto otro".

		Para este segundo tipo de decisiones, existe una variante de la estructura if llamada if...else  como la mostramos en el editor. Si la condición se cumple (es decir, si su valor es true) se ejecutan todas las instrucciones que se encuentran dentro del if(). Si la condición no se cumple (es decir, si su valor es false) se ejecutan todas las instrucciones contenidas en else { }. Si el valor de la variable edad es mayor o igual que el valor numérico 18, la condición del if() se cumple y por tanto, se ejecutan sus instrucciones y se muestra el mensaje "Eres mayor de edad". Sin embargo, cuando el valor de la variable edad no es igual o mayor que 18, la condición del if() no se cumple, por lo que automáticamente se ejecutan todas las instrucciones del bloque else { }. En este caso, se mostraría el mensaje "Todavía eres menor de edad".

Clase 11:

	Título:

		Estructura if (parte 4)

	Código:

		var edad = prompt("Ingresa tu edad")
		if(edad < 12) {
		alert("Todavía eres muy pequeño");
		}
		else if(edad < 19) {
		alert("Eres un adolescente");
		}
		else if(edad < 35) {
		alert("Aun sigues siendo joven");
		}
		else {
		alert("Debes Ingresar un número");
		}

	Documentación:
	
		Una vez mas, hemos agregado una instrucción para que el usuario pueda ingresar un valor, pero esta vez, le damos la libertad de ingresar por teclaso lo que él desee. Con este ejemplo, concatenamos varias instrucciones if... else para realizar diferentes comprobaciones e incluso un caso dónde el usuario esta ingresando un valor que no´entra dentro de los parámetros que esperamos recibir. 

Clase 12:
	
	Título:
		Estructura for

	Código: 

		var mensaje = "Hola, estoy dentro de un bucle";
		for(var i = 0; i < 5; i++) {
			alert(mensaje+", este memsaje se ha mostrado "+(i+1)+" veces");
		}

	Documentación:

		Las estructuras if y if...else no son muy eficientes cuando se desea ejecutar de forma repetitiva una instrucción. La estructura for permite realizar repeticiones (también llamadas bucles) de una forma muy sencilla. No obstante, su definición formal no es tan sencilla como la de if() que vimos en las últimas lecciones, la estructura for se declara de la siguiente manera:

		for(inicializacion; condicion; actualizacion) { ... }

		La idea del funcionamiento de un bucle for es la siguiente: "mientras la condición indicada se siga cumpliendo, repite la ejecución de las instrucciones definidas dentro del for. Además, después de cada repetición, actualiza el valor de las variables que se utilizan en la condición". 

		La "inicialización" es la zona en la que se establece los valores iniciales de las variables que controlan la repetición.
		La "condición" es el único elemento que decide si continua o se detiene la repetición. 
		La "actualización" es el nuevo valor que se asigna después de cada repetición a las variables que controlan la repetición

		La parte de la inicialización del bucle consiste en: var i = 0; . Por tanto, en primer lugar se crea la variable i y se le asigna el valor de 0. Esta zona de inicialización solamente se tiene en consideración justo antes de comenzar a ejecutar el bucle. Las siguientes repeticiones no tienen en cuenta esta parte de inicialización. La zona de condición del bucle es: i < 5; . Los bucles se siguen ejecutando mientras se cumplan las condiciones y se dejan de ejecutar justo después de comprobar que la condición no se cumple. En este caso, mientras la variable i valga menos de 5 el bucle se ejecuta indefinidamente. Como la variable i se ha inicializado a un valor de 0 y la condición para salir del bucle es que i sea menor que 5, si no se modifica el valor de i de alguna forma, el bucle se repetiría indefinidamente. Por ese motivo, es imprescindible indicar la zona de actualización, en la que se modifica el valor de las variables que controlan el bucle: i++ En este caso, el valor de la variable i se incrementa en una unidad después de cada repetición. La zona de actualización se ejecuta después de la ejecución de las instrucciones que incluye el for. Así, durante la ejecución de la quinta repetición el valor de i será 4. 
		Después de la quinta ejecución, se actualiza el valor de i, que ahora valdrá 5. Como la condición es que i sea menor que 5, la condición ya no se cumple y las instrucciones del for no se ejecutan una sexta vez. Normalmente, la variable que controla los bucles for se llama i, ya que recuerda a la palabra índice y su nombre tan corto ahorra mucho tiempo y espacio.

Clase 13:

	Título:

		Estructura for...in

	Código:

		var dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
		var i = 0;
		for(i in dias) {
			alert(dias[i]);
		}


	Documentación:

		Una estructura de control derivada de for es la estructura for...in. Su definición exacta implica el uso de objetos, que es un elemento de programación avanzada que no se va a estudiar. Por tanto, solamente se va a presentar la estructura for...in adaptada a su uso en arrays. Su definición formal adaptada a los arrays es: for(indice in array) { ... } . 

		Si se quieren recorrer todos los elementos que forman un array, la estructura for...in es la forma más eficiente de hacerlo, como se muestra en el ejemplo.

		La variable que se indica como indice es la que se puede utilizar dentro del bucle for...in para acceder a los elementos del array. De esta forma, en la primera repetición del bucle la variable i vale 0 y en la última vale 6. Esta estructura de control es la más adecuada para recorrer arrays (y objetos), ya que evita tener que indicar la inicialización y las condiciones del bucle for simple y funciona correctamente cualquiera que sea la longitud del array. De hecho, sigue funcionando igual aunque varíe el número de elementos del array.

Clase 14:

	Título:
		Funciones útiles para cadenas de texto
	
	Código:

		var mensaje = "Hola Mundo";
		var numeroLetras = mensaje.length; 
		// numeroLetras = 10

		var mensaje1 = "Hola";
		var mensaje2 = " Mundo";
		var mensaje_unido = mensaje1 + mensaje2; 
		// mensaje_unido = "Hola Mundo"

		var mensaje_concatenado = mensaje1.concat(" Mundo"); 
		// mensaje_concatenado = "Hola Mundo"

		var variable1 = "Hola ";
		var variable2 = 3;
		var variable_mensaje = variable1 + variable2; 
		// variable_mensaje = "Hola 3"

		var mensaje_mayusculas = mensaje1.toUpperCase(); 
		// mensaje_mayusculas = "HOLA"
		var mensaje_minusculas = mensaje1.toLowerCase(); 
		// mensaje_minusculas = "hola"

		var letra1 = mensaje.charAt(0); 
		// letra1 = H
		letra2 = mensaje.charAt(2); 
		// letra2 = l
		
		var posicion = mensaje.indexOf('a'); 
		// posicion = 3
		posicion = mensaje.indexOf('b'); 
		// posicion = -1

		var porcion_1 = mensaje.substring(2); 
		// porcion_1 = "la Mundo"
		porcion_2 = mensaje.substring(5); 
		// porcion_2 = "Mundo"
		porcion_3 = mensaje.substring(1,8); 
		// porcion_3 = "ola Mun"

		var mensaje_extendido = "Hola Mundo, soy una cadena de texto!";
		var palabras = mensaje_extendido.split(" ");
		// palabras = ["Hola", "Mundo,", "soy", "una", "cadena", "de", "texto!"];
		
		var palabra = "Hola";
		var letras = palabra.split(""); 
		// letras = ["H", "o", "l", "a"]


	Documentación:

		JavaScript incorpora una serie de herramientas y utilidades (llamadas funciones y propiedades) para el manejo de las variables. De esta forma, muchas de las operaciones básicas con las variables, se pueden realizar directamente con las utilidades que ofrece JavaScript.

		A continuación se muestran algunas de las funciones más útiles para el manejo de cadenas de texto:

		length : calcula la longitud de una cadena de texto (el número de caracteres que la forman).
		
		+ : como ya habiamos visto en ejemplos anteriores se emplea para concatenar varias cadenas de texto.Además del operador +, también se puede utilizar la función concat(). Las cadenas de texto también se pueden unir con variables numéricas.

		toUpperCase(), transforma todos los caracteres de la cadena a sus correspondientes caracteres en mayúsculas.
		
		toLowerCase(), transforma todos los caracteres de la cadena a sus correspondientes caracteres en minúsculas.
		
		charAt(posicion), obtiene el carácter que se encuentra en la posición indicada.

		indexOf(caracter), calcula la posición en la que se encuentra el carácter indicado dentro de la cadena de texto. Si el carácter se incluye varias veces dentro de la cadena de texto, se devuelve su primera posición empezando a buscar desde la izquierda. Si la cadena no contiene el carácter, la función devuelve el valor -1.

		La función lastIndexOf() comienza su búsqueda desde el final de la cadena hacia el principio, aunque la posición devuelta es la correcta empezando a contar desde el principio de la palabra. 

		substring(inicio, final), extrae una porción de una cadena de texto. El segundo parámetro es opcional. Si sólo se indica el parámetro inicio, la función devuelve la parte de la cadena original correspondiente desde esa posición hasta el final.

		split(separador), convierte una cadena de texto en un array de cadenas de texto. La función parte la cadena de texto determinando sus trozos a partir del carácter separador indicado.

Clase 15:

	Título:
		 Funciones útiles para arrays
	
	Código:

		var vocales = ["a", "e", "i", "o", "u"];
		var numeroVocales = vocales.length; 
		// numeroVocales = 5

		var array1 = [1, 2, 3];
		array2 = array1.concat(4, 5, 6); 
		// array2 = [1, 2, 3, 4, 5, 6]
		array3 = array1.concat([4, 5, 6]); 
		// array3 = [1, 2, 3, 4, 5, 6]

		var array4 = ["hola", "mundo"];
		var mensaje_unido = array4.join(""); 
		// mensaje_unido = "holamundo"
		mensaje_con_espacio = array4.join(" "); 
		// mensaje_con_espacio = "hola mundo"

		var array5 = [1, 2, 3];
		var ultimo = array5.pop();
		// ahora array5 = [1, 2], ultimo = 3
		
		var array6 = [10, 20, 30];
		array6.push(40);
		// ahora array6 = [10, 20, 30, 40]

		var array7 = [1, 2, 3];
		var primero = array7.shift();
		// ahora array7 = [2, 3], primero = 1

		var array8 = [1, 2, 3];
		array8.unshift(0);
		// ahora array8 = [0, 1, 2, 3] 

		var array9 = [1, 2, 3];
		array9.reverse();
		// ahora array9 = [3, 2, 1]

	Documentación:

		Así como vimos con las cadenas de texto, las funciones length y concat funcionan de manera muy similar con los arreglos, asi como lo demostramos en el editor.

		join(separador), es la función contraria a split(). Une todos los elementos de un array para formar una cadena de texto. Para unir los elementos se utiliza el carácter separador indicado

		pop(), elimina el último elemento del array y lo devuelve. El array original se modifica y su longitud disminuye en 1 elemento.

		push(), añade un elemento al final del array. El array original se modifica y aumenta su longitud en 1 elemento. (También es posible añadir más de un elemento a la vez)

		shift(), elimina el primer elemento del array y lo devuelve. El array original se ve modificado y su longitud disminuida en 1 elemento.

		unshift(), añade un elemento al principio del array. El array original se modifica y aumenta su longitud en 1 elemento. (También es posible añadir más de un elemento a la vez).

		reverse(), modifica un array colocando sus elementos en el orden inverso a su posición original.

Clase 16:

	Título:
		 Funciones útiles para números
	
	Código:

		var numero1 = 0;
		var numero2 = 0;

		if(isNaN(numero1/numero2)) {
			
			alert(numero1/numero2); 
			// se muestra el valor NaN

			alert("La división no está definida para los números indicados");
		}
		else {
			alert("La división es igual a => " + numero1/numero2);
		}

		var numero_aprox = 4564.34567;
		aproximacion_1 = numero_aprox.toFixed(2); 
		// aproximacion_1 = 4564.35
		aproximacion_2 = numero_aprox.toFixed(6); 
		// aproximacion_2 = 4564.345670
		aproximacion_3 = numero_aprox.toFixed(); 
		// aproximacion_3 = 4564


	Documentación:

		NaN, (del inglés, "Not a Number") JavaScript emplea el valor NaN para indicar un valor numérico no definido (por ejemplo, la división 0/0) mientras que isNaN(), permite proteger a la aplicación de posibles valores numéricos no definidos.

		Infinity, hace referencia a un valor numérico infinito y positivo (también existe el valor –Infinity para los infinitos negativos)

		toFixed(digitos), devuelve el número original con tantos decimales como los indicados por el parámetro digitos y realiza los redondeos necesarios. Se trata de una función muy útil por ejemplo para mostrar precios.